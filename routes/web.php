<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/profil', 'HomeController@index');

Route::resource('ankete', 'SurveyController');
Route::post('/survey_answers', 'SurveyAnswerController@store')->middleware('auth');
Route::post('/survey_questions', 'SurveyQuestionController@store')->middleware('auth');

Route::get('/predmeti/moji_predmeti', 'SubjectController@mojiPredmeti');
Route::post('/predmeti/moji_predmeti', 'SubjectController@dodajMojPredmet');
Route::delete('/predmeti/moji_predmeti', 'SubjectController@ukloniMojPredmet');

Route::resource('predmeti', 'SubjectController');

//Route::get('/predmeti/{subject}/materijali', 'MaterialController@index');
//Route::get('/materijali/create', 'MaterialController@create');
//Route::get('/materijali/{material}', 'MaterialController@show');
//Route::post('/predmeti/{subject}/materijali', 'MaterialController@store');
//Route::post('/materijali', 'MaterialController@store');
Route::resource('/predmeti/{subject}/materijali', 'MaterialController');


//Route::get('/pitanja', 'QuestionController@index');
Route::get('/predmeti/{subject}/pitanja/create', 'QuestionController@create');
Route::get('/predmeti/{subject}/pitanja/{question}', 'QuestionController@show');
//Route::post('/pitanja', 'QuestionController@store');

Route::resource('/predmeti/{subject}/obavjestenja', 'NotificationController');

Route::resource('profesori', 'TeacherController');

Route::resource('pitanja', 'QuestionController');

Route::resource('smjerovi', 'DepartmentController');

Route::get('fajlovi/{id}', 'AttachmentController@show');

Route::post('/odgovori', 'QuestionController@storeReply');