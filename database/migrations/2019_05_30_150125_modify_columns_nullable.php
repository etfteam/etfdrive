<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ModifyColumnsNullable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //Schema::table('users', function (Blueprint $table) {
        //    $table->boolean('admin')->default(false)->change();
        //});
        Schema::table('subjects', function (Blueprint $table) {
            $table->string('text')->nullable()->change();
        });
        Schema::table('materials', function (Blueprint $table) {
            $table->string('description')->nullable()->change();
            $table->unsignedBigInteger('parent_id')->nullable()->change();
            $table->boolean('folder')->default(false)->change();
            
        });
        Schema::table('questions', function (Blueprint $table) {
            $table->unsignedBigInteger('answer_to')->nullable()->change();
        });
        Schema::table('attachments', function (Blueprint $table) {
            $table->string('description')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->boolean('admin')->change();
        });
        Schema::table('subjects', function (Blueprint $table) {
            $table->string('text')->change();
        });
        Schema::table('materials', function (Blueprint $table) {
            $table->string('description')->change();
            $table->unsignedBigInteger('parent_id')->change();
            $table->boolean('folder')->change();
            
        });
        Schema::table('questions', function (Blueprint $table) {
            $table->unsignedBigInteger('answer_to')->change();
        });
        Schema::table('attachments', function (Blueprint $table) {
            $table->string('description')->change();
        });
    }
}
