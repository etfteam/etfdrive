<?php

use Illuminate\Database\Seeder;

class SurveyAnswerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('survey_answers')->insert([
            'survey_question_id' => 1,
            'answer' => array_rand (['A'=>'A','B'=>'B','C'=>'C','D'=>'D','E'=>'E']),
            'user_id' => rand(3,30),
        ]);
    }
}
