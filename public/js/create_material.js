/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 4);
/******/ })
/************************************************************************/
/******/ ({

/***/ "./resources/js/create_material.js":
/*!*****************************************!*\
  !*** ./resources/js/create_material.js ***!
  \*****************************************/
/*! no static exports found */
/***/ (function(module, exports) {

window.addEventListener('load', function () {
  function addSubject(event) {
    event.preventDefault();
    var subjectBox = $('.subject_template > div').clone();
    var subjectId = $(this).next().text();
    var subjectName = $(this).next().next().text();
    console.log(subjectName);
    subjectBox.click(removeSubject);
    subjectBox.html('<input name="predmeti[' + subjectId + ']" value="' + subjectId + '" hidden>' + subjectName);
    subjectBox.appendTo('#odabrani_predmeti');
  }

  function removeSubject(event) {
    event.preventDefault();
    $(this).detach();
  }

  function odaberi(event) {
    var godina = event.data.godina;

    for (var i = 1; i <= 4; i++) {
      if (i === godina) {
        $('.godina_' + i).prop('hidden', false);
        $('#b' + i).addClass('godina-dugme-izabrano');
      } else {
        $('.godina_' + i).prop('hidden', true);
        $('#b' + i).removeClass('godina-dugme-izabrano');
      }
    }

    if (godina == 1 || godina == 2 || godina == 3) {
      console.log(godina);
      $('.nivo_osnovne').prop('hidden', false);
      $('.nivo_spec').prop('hidden', true);
    }

    if (godina === 4) {
      $('.nivo_osnovne').prop('hidden', true);
      $('.nivo_spec').prop('hidden', false);
    }
  }

  $('.fa-plus').click(addSubject);
  $('#b1').click({
    godina: 1
  }, odaberi);
  $('#b2').click({
    godina: 2
  }, odaberi);
  $('#b3').click({
    godina: 3
  }, odaberi);
  $('#b4').click({
    godina: 4
  }, odaberi);
});

/***/ }),

/***/ 4:
/*!***********************************************!*\
  !*** multi ./resources/js/create_material.js ***!
  \***********************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! /home/andrej/code/etfdrive/resources/js/create_material.js */"./resources/js/create_material.js");


/***/ })

/******/ });