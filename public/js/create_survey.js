/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 0);
/******/ })
/************************************************************************/
/******/ ({

/***/ "./resources/js/create_survey.js":
/*!***************************************!*\
  !*** ./resources/js/create_survey.js ***!
  \***************************************/
/*! no static exports found */
/***/ (function(module, exports) {

window.addEventListener('load', function () {
  var numQuestions = 0;

  function addQuestion() {
    if ($('#tip_pitanja').val() == "text") {
      var questionForm = $('#question_form_template > form').clone();
    } else {
      var questionForm = $('#question_form_template_multi > form').clone();
      handleChoices(questionForm.find('.ponudjeni_odgovori'), numQuestions);
    }

    questionForm.find('#btn_remove_question').click(removeQuestion);
    questionForm.appendTo('#questions');
    numQuestions++;
  }

  function handleChoices(choicesDiv, numQuestions) {
    var numChoices = 0;
    choicesDiv.children('button').click(function (event) {
      event.preventDefault();
      console.log(numChoices);
      $(this).parent().append('<input type="text" name="choices[' + numChoices++ + ']" class="form-control bg-lightgray mt-2">');
    });
  }

  function removeQuestion(event) {
    event.preventDefault();
    $(this).closest('form').remove();
  }

  function submitSurvey() {
    $(this).hide();
    $('#sending_loader').show();
    $.ajax({
      type: "POST",
      url: "/ankete",
      data: $('#survey').serialize(),
      success: function success(data) {
        submitQuestions(data);
      }
    });
    $(document).ajaxStop(function () {
      window.location.replace('/ankete');
    });
  }

  function submitQuestions(data) {
    $('.question_form').each(function () {
      $.ajax({
        type: "POST",
        url: "/survey_questions",
        data: $(this).serialize() + "&survey_id=" + data.id,
        success: function success(data) {
          console.log(data);
        }
      });
    });
  }

  $('#btn_add_question').click(addQuestion); //$('#btn_remove_question').click(removeQuestion());

  $('#btn_submit').click(submitSurvey);
});

/***/ }),

/***/ "./resources/sass/app.scss":
/*!*********************************!*\
  !*** ./resources/sass/app.scss ***!
  \*********************************/
/*! no static exports found */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),

/***/ 0:
/*!***********************************************************************!*\
  !*** multi ./resources/js/create_survey.js ./resources/sass/app.scss ***!
  \***********************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(/*! /home/andrej/code/etfdrive/resources/js/create_survey.js */"./resources/js/create_survey.js");
module.exports = __webpack_require__(/*! /home/andrej/code/etfdrive/resources/sass/app.scss */"./resources/sass/app.scss");


/***/ })

/******/ });