window.addEventListener('load', function () {
    $.ajax({
      url: "ankete/"+surveyId+"/results",
      method: "GET",
      success: function(data) {
        console.log(data);
        var odgovori = [];
        var brOdgovora = [];
  
        for(var i in data) {
          odgovori.push("Player " + data[i].playerid);
          score.push(data[i].score);
        }
  
        var chartdata = {
          labels: [],
          datasets : [
            {
              label: 'Player Score',
              backgroundColor: 'rgba(200, 200, 200, 0.75)',
              borderColor: 'rgba(200, 200, 200, 0.75)',
              hoverBackgroundColor: 'rgba(200, 200, 200, 1)',
              hoverBorderColor: 'rgba(200, 200, 200, 1)',
              data: score
            }
          ]
        };
  
        var ctx = $("#mycanvas");
  
        var barGraph = new Chart(ctx, {
          type: 'bar',
          data: chartdata
        });
      },
      error: function(data) {
        console.log(data);
      }
    });
  });