window.addEventListener('load', function () {
    var numQuestions=0;
    function addQuestion () {
        if($('#tip_pitanja').val() == "text"){
            var questionForm = $('#question_form_template > form').clone();    
        } else {
            var questionForm = $('#question_form_template_multi > form').clone();
            handleChoices(questionForm.find('.ponudjeni_odgovori'), numQuestions);    
        }        
        questionForm.find('#btn_remove_question').click(removeQuestion);
        questionForm.appendTo('#questions');
        numQuestions++;
    }
    function handleChoices(choicesDiv, numQuestions) {
        var numChoices = 0;
        choicesDiv.children('button').click(function(event) {
            event.preventDefault();
            console.log(numChoices);
            $(this).parent().append('<input type="text" name="choices['+(numChoices++)+ 
                ']" class="form-control bg-lightgray mt-2">');    
        });
        
    } 
    function removeQuestion(event) {
        event.preventDefault();
        $(this).closest('form').remove();
    }
    function submitSurvey() {
        $(this).hide();
        $('#sending_loader').show();
        $.ajax({
            type: "POST",
            url: "/ankete",
            data: $('#survey').serialize(),
            success: function(data){submitQuestions(data)},
        });

        $(document).ajaxStop(function () {
            window.location.replace('/ankete');
        });
    }
    function submitQuestions(data) {
        $('.question_form').each(function () {
            $.ajax({
                type: "POST",
                url: "/survey_questions",
                data: $(this).serialize()+"&survey_id="+data.id,
                success: function(data) {
                    console.log(data);
                },
            });
        });
    }
    $('#btn_add_question').click(addQuestion);
    //$('#btn_remove_question').click(removeQuestion());
    $('#btn_submit').click(submitSurvey);
});