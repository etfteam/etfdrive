window.addEventListener('load', function () {
    function submitAnswers() {
        $('#btn_submit_answers').hide();
        $('.answer_form').each(function () {
            $.ajax({
                type: "POST",
                url: "/odgovori",
                data: $(this).serialize(),
                success: function(data) {
                    $('btn_submit_answers').show();
                    $("input[type=text], textarea").val("");
                    //answerDiv=$('#answer-template > div').clone();
                    //answerDiv.find('h4').html(data.title);
                    //answerDiv.find('p').html(data.text);
                    //answerDiv.appendTo('.noviOdgovor');l
                    location.reload();
                },
            });
        });
    }
    $('#btn_submit_answers').click(submitAnswers);
});