window.addEventListener('load', function () {
    function submitAnswers() {
        $(this).hide();
        $('#loader').show();
        $('.answer_form').each(function () {
            var answer={};
            var tip_odgovora=$(this).children('.tip_odgovora').text();
            answer['survey_question_id'] = $(this).find('input[name="survey_question_id"]').val();
            if(tip_odgovora=="text") answer['answer'] = $(this).find('input[name="answer"]').val();
            else answer['answer'] = $(this).find('input[name="answer"]:checked').val();
            console.log(answer);
            $.ajax({
                type: "POST",
                url: "/survey_answers",
                data: answer,
                success: function(data) {
                    window.location.href = "/ankete";
                },
            });
        });
    }
    $('#btn_submit_answers').click(submitAnswers);
});