window.addEventListener("load", function () {
    function odaberi(event) {
        let godina = event.data.godina;
        for(let i=1; i<=4;i++){
            if(i===godina) {
                $('.godina_'+i).prop('hidden',false);
                $('#b'+i).addClass('godina-dugme-izabrano');
            }
            else {
                $('.godina_'+i).prop('hidden',true);
                $('#b'+i).removeClass('godina-dugme-izabrano');
            }
        }
        if(godina==1 || godina==2 || godina==3) {
            console.log(godina);
            $('.nivo_osnovne').prop('hidden', false);
            $('.nivo_spec').prop('hidden', true);
        }
        if(godina===4){
            $('.nivo_osnovne').prop('hidden', true);
            $('.nivo_spec').prop('hidden', false);
        }

    }
    function dodajPredmet(predmet) {
        $('#ul_predmeti')
            .append('<li><i class="fas fa-minus">&nbsp;&nbsp;</i>'+
                '<a href="/predmeti/'+predmet['id']+'">'+predmet['name']+'</a>'+
                '<span hidden>'+predmet['id']+'</span></li>');
    }
    function dodajPredmetIzSvih() { 
        predmetiLoader.show();
        var predmet={
            id: $(this).next().text(),
            name: $(this).next().next().text()
        };
        $.ajax({
                type: "POST",
                url: "/predmeti/moji_predmeti",
                data: {id: predmet['id']},
                success: function(data) {
                    dodajPredmet(predmet);
                    predmetiLoader.hide();
                },
        });
    }
    function ukloniPredmet() {
        predmetiLoader.show();
        var predmetId = $(this).next().text();
        var predmetLi = $(this).parent();
        $.ajax({
                type: "DELETE",
                url: "/predmeti/moji_predmeti",
                data: {id: predmetId},
                success: function(data) {
                    predmetLi.remove();
                    predmetiLoader.hide();
                },
        });
    }
    function ucitajPredmete() {         
        $.ajax({
                type: "GET",
                url: "/predmeti/moji_predmeti",
                data: {},
                success: function(data) {
                    predmetiLoader.hide();
                    data.map(predmet => dodajPredmet(predmet));
                },
        });
    }

    var predmetiLoader = $('#predmeti-loader');
    $(document).on('click', '.fa-minus', ukloniPredmet);
    $('.fa-plus').click(dodajPredmetIzSvih);
    $('#b1').click({godina:1}, odaberi);
    $('#b2').click({godina:2}, odaberi);
    $('#b3').click({godina:3}, odaberi);
    $('#b4').click({godina:4}, odaberi);

    if($('#korisnik_ulogovan').length){
        ucitajPredmete();
    }

});