window.addEventListener('load', function () {
    function addDepartment(event) {
        event.preventDefault();
        var departmentBox = $('.department_template > div').clone();
        var departmentId = $(this).prev().children('option:selected').val();
        var departmentName = $(this).prev().children('option:selected').text();
        console.log(departmentName);
        departmentBox.click(removeDepartment);
        departmentBox.html('<input name="departments['+departmentId+
            ']" value="'+departmentId+'" hidden>' +
            departmentName);
        departmentBox.appendTo('#smjerovi');
    }
    function removeDepartment(event) {
        event.preventDefault();
        $(this).detach();
    }
    $('#dodaj_smjer').click(addDepartment);
});