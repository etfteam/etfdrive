@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-sm-6">
            <form method="post" action="/profesori">
                @csrf
                @method('PATCH')
                <div class="form-group">
                    <label for="name">Ime i prezime:</label>
                    <input type="text" id="name" name="name" class="form-control" 
                    placeholder="Unesi ime i prezime profesora" 
                    value="{{ $teacher->name }}" required>
                </div>
                <div class="form-group">
                    <label for="email">E-mail:</label>
                    <input type="email" id="email" name="email" class="form-control" 
                    placeholder="Unesi e-mail adresu profesora" 
                    value="{{ $teacher->email }}" required>
                </div>
                <div class="form-group">
                    <label for="kabinet">Kabinet:</label>
                    <input type="text" id="kabinet" name="kabinet" class="form-control" 
                    placeholder="Unesi broj kabineta profesora"
                    value="{{ $teacher->kabinet }}" required>
                </div>
                <div class="form-group">
                    <button type="submit" class="btn btn-primary col-sm-2">Pošalji</button>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection