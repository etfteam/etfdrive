@extends('layouts.app')

@section('content')
<div class="container">
    <h1 class="h1 mb-4">Profesori</h1>
        <div class="d-flex flex-wrap justify-content-center justify-content-lg-start">
        @foreach($teachers as $teacher)            
            <div class="card m-2 d-flex px-3" style="flex: 0 0 16rem; height: 16rem">
                <div class="card-body overflow-hidden">
                    <h3 class="card-title">{{ $teacher->name }}</h3>
                    <p class="card-text">E-mail: {{ $teacher->email }}</p>
                    <p class="card-text">Kabinet: {{ $teacher->kabinet }}</p>
                </div>
                <div class="card-footer bg-transparent text-right">
                    <a class="btn btn-secondary" href="/profesori/{{ $teacher->id }}/edit">
                        <i class="fas fa-edit"></i></a>
                    <form action="/profesori/{{ $teacher->id }}"  class="d-inline" method="post">
                        @csrf
                        @method('DELETE')
                        <button class="btn btn-danger">
                            <i class="fas fa-trash"></i></button>
                    </form>                                                
                </div>
            </div>           
        @endforeach
        <div class="card m-2" style="flex: 0 0 16rem; height: 16rem">
            <div class="card-body col d-flex flex-column justify-content-center 
            align-items-center bg-lightgray">
                <a href="/profesori/create" class=" display-3 text-secondary"><i class="fas fa-plus"></i></a>
            </div>
        </div>
        </div>
</div>
@endsection