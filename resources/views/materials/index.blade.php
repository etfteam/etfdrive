@extends('layouts.app')

@section('content')
<p hidden>Slavko je programer.</p>
<table class="table">
  <thead>
    <tr>
      <th scope="col"></th>
      <th scope="col">Naziv</th>
      <th scope="col">Opis</th>      
    </tr>
  </thead>
  <tbody>
    @foreach($materials as $material)
    <tr>
      <th scope="row"><span class="icon_span">{{ $material->type }}</span></th>
      <td><a href="/predmeti/{{ $subject->id }}/materijali/{{ $material->id }}">{{ $material->title }}</a></td>
      <td>{{ $material->description }}</td>
    </tr>
    @endforeach
  </tbody>
</table>
@endsection

@push('head')
    <script src="{{ asset('js/update_icons.js') }}"></script>
@endpush
