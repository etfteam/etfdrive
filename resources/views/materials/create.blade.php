@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
    <div class="col-sm-6">
    <h1>{{ $subject->name }}</h1>
    <form method="post" action="/predmeti/{{ $subject->id }}/materijali" enctype="multipart/form-data">
        @csrf
        <input type="hidden" name="subject_id" value="{{ $subject->id }}">
        <div class="form-group">
            <label for="name">Naziv materijala:</label>
            <input type="text" id="name" name="title" class="form-control" placeholder="Unesi naziv materijala" required>
        </div>
        <div class="form-group">
            <label for="text">Opis materijala:</label>
            <textarea rows="3" id="text" name="description" class="form-control"></textarea>
        </div>
        <!--<div class="form-group">
            <label>Predmeti:</label>
            <small class="form-text text-muted">Odaberi predmete za koje je vezan materijal.</small>
            <div id="odabrani_predmeti"></div>    
        </div>-->        
        <div class="form-group">
          <p>Fajl:</p>
          <div class="custom-file"> 
            <input type="file" class="custom-file-input" name="attachment" id="validatedCustomFile" required>
            <label class="custom-file-label" for="validatedCustomFile">Odaberi fajl...</label>
          </div>
        </div>
        <div class="form-group">
            <button type="submit" class="btn btn-primary col-sm-3">Pošalji</button>
        </div>        
    </form>
</div>
</div>
</div>

@endsection

