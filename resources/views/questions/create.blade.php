@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-sm-6">
            <form method="post" action="/pitanja" enctype='multipart/form-data'>
                @csrf
                <div class="form-group">
                    <label for="name">Postavi pitanje:</label>
                    <input name="title" type="text" class="form-control" placeholder="Naslov">
                </div>
                <div class="form-group">
                    <textarea name="text" class="form-control" placeholder="Tekst pitanja"></textarea>
                </div>
                <div class="form-group">
                    <div class="custom-file">
                        <input name="fajl" type="file" class="custom-file-input" id="customFile">
                        <label class="custom-file-label" for="customFile">Izaberi fajl</label>
                    </div>
                </div>
                <div class="form-group">
                    <button class="btn btn-danger col-sm-3" id="btn_remove_question">Remove</button>
                </div>
                <input type="hidden" value="{{ $subject->id }}" name="subject_id">
                <div class="form-group">
                    <button id="btn_submit" class="btn btn-primary col-sm-3">Posalji</button>
                </div>

            </form>
        </div>
    </div>
</div>
@endsection

@push('head')
    <script src="{{ asset('js/create_question.js') }}"></script>
@endpush