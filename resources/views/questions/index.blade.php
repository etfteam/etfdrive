@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row justify-content-center">
        <div class="col-md-8">
        @foreach($questions as $question)
            @if($question->answer_to==null)
            <div class="card col-sm-12 mt-2">
                <div class="card-body">
                    <span>
                        <small class="mr-3">{{ $question->user()->first()->name }}</small>                       
                        <small class="text-muted">{{ $question->subject()->first()->name }}</small>
                        <small class="float-right">{{ date('d.m.Y H:i', strtotime($question->created_at)) }}</small>
                    </span>
                    <hr class="my-0 mb-2">
                    <h4 class="card-title" style="color: #d66e0a">{{$question->title}}</h4>
                    <p class="card-text">{{$question->text}}</p>

                </div>
                <div class="card-footer text-right bg-transparent bezok">
                    <a href="pitanja/{{ $question->id }}" class="btn btn-primary">Odgovori</a>
                </div>
            </div>
            @endif
        @endforeach
        </div>
        </div>
    </div>
@endsection