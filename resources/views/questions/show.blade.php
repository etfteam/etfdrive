@extends('layouts.app')

@section('content')
<div class="container">
    <div class="justify-content-center row">
        <div class="col-md-8">
            <div class="card bg-dark text-light col-sm-12 mt-2">
                <div class="card-body">
                    <span class="text-light">
                        <small class="mr-3">{{ $question->user()->first()->name }}</small>
                        <small class="text-muted">{{ $question->subject()->first()->name }}</small>
                        <small class="float-right">{{ date('d.m.Y H:i', strtotime($question->created_at)) }}</small>
                    </span>
                    <hr class="my-0 mb-1 border-secondary">
                    <h4 class="card-title">{{$question->title}}</h4>
                    <p class="card-text">{{$question->text}}</p>
                    @if(count($attachments)>0)
                    <p class="text-light mt-4 mb-0">Priloženi fajlovi:</p>
                    @endif
                    @foreach($attachments as $attachment)
                        <a href="/fajlovi/{{ $attachment->id }}">
                            <span class="text-light"><i class="fas fa-file">&nbsp;&nbsp;</i>
                                <small>{{ $question->path }}</small>
                            </span>
                        </a>
                    @endforeach
                    @if(Auth::id() == $question->user()->first()->id)
                    <div class="w-100 text-right">
                        <form method="POST" action="/pitanja/{{ $question->id }}">
                            @csrf
                            @method('DELETE')
                            <button class="notButton" type="submit"><i class="fas fa-trash"></i></button>    
                        </form>
                        
                    </div>
                    @endif
                </div>
            </div>
            @foreach($answers as $answer)
            <div class="card col-sm-12 mt-2">
                <div class="card-body">                    
                    <span class="text-muted">
                        <small>{{ $answer->user()->first()->name }}</small>
                        <small class="float-right">{{ date('d.m.Y H:i', strtotime($answer->created_at)) }}</small>
                    </span>
                    <hr class="my-0 mb-1">
                    <h4 class="card-title">{{ $answer->title }}</h4>
                    <p class="card-text">{{ $answer->text }}</p>
                    @if(Auth::id() == $answer->user()->first()->id)
                    <div class="w-100 text-right">
                        <form method="POST" action="/pitanja/{{ $answer->id }}">
                            @csrf
                            @method('DELETE')
                            <button class="notButton" type="submit"><i class="fas fa-trash"></i></button>    
                        </form>
                    </div>
                    @endif
                </div>
            </div>
            @endforeach
            <div class="noviOdgovor"></div>
            <div class="card col-sm-12 mt-2">
                <div class="card-header bg-transparent">
                    Novi odgovor
                </div>
                <div class="card-body">
                    <form class="answer_form">
                        @csrf
                        <div class="form-group form-group-lg">
                            <label class="form-control shadow-none bezok mr-2" for="odgovor">Naslov odgovora:</label>
                            <input
                                name="title"
                                type="text"
                                placeholder="Odgovor" class="form-control">
                            <input type="hidden" name="answer_to" class="form-control" 
                                value="{{ $question->id }}">
                            <input type="hidden" name="subject_id" class="form-control" 
                                value="{{ $question->subject_id }}">

                        </div>
                        <div class="form-group form-group-lg">
                            <label class="form-control shadow-none bezok mr-2" for="odgovor">Tekst odgovora:</label>
                            <textarea name="text" class="form-control"></textarea>
                        </div>
                    </form>        
                </div>
            </div>                    
            <div class="form-group">
                <button id="btn_submit_answers" class="btn btn-primary col-sm-3  mt-2">Posalji</button>
            </div>
        </div>
    </div>
</div>
<div id="answer-template" hidden>
    <div class="card col-sm-12 px-2 mt-2">
        <div class="card-header bg-transparent">
            <h4 class="card-title mb-0"></h4>
        </div>
        <div class="card-body">            
            <p class="card-text"></p>
        </div>
    </div>
</div>
@endsection

@push('head')
    <script src="{{ asset('js/answer_question.js') }}"></script>
    <script src="{{ asset('js/update_icons.js') }}"></script>
@endpush