@extends('layouts.app')

@section('content')
<div class="container">
    <div class="justify-content-center row">
        <div class="col-lg-6">
            <div class="card px-3 mb-3">
                <div class="card-header bg-transparent">
                    Moj profil

                </div>
                <div class="card-body">
                    <h3 class="mb-4">
                        {{ Auth::user()->name }}
                        @if(Auth::user()->admin)
                            <small class="text-muted"><small>(admin)</small></small>
                        @endif
                    </h3>
                    
                    <p><strong>E-mail:</strong></p>
                    <p>{{ Auth::user()->email }}</p>
                    
                    <p><strong>Godina studija:</strong></p>
                    <p>{{ Auth::user()->year }}</p>

                    <p><strong>Smjer:</strong></p>
                    <p>@php  
                        $department=App\Department::find(Auth::user()->department_id);
                        if($department!=null){
                            echo $department->name;
                        } 
                    @endphp</p>
                </div>
            </div>
        </div>
        @if(Auth::user()->admin)
        <div class="col-lg-6">
            <div class="card px-3 d-flex" style="height: 80vh">
                <div class="card-header bg-transparent">Administracija</div>
                <div class="card-body d-flex flex-column h-50">
                    <h3 class="mb-4">Promjena podataka</h3>
                    <span>
                        <a class="btn btn-primary" href="/smjerovi">Smjerovi</a>
                        <a href="/profesori" class="btn btn-primary">Profesori</a>
                        <a href="/predmeti" class="btn btn-primary">Predmeti</a>
                    </span>
                    <p>&nbsp;</p>
                    <h3 class="mb-4">Registrovani korisnici</h3>                    
                    <table class="table h-50 table-responsive" style="flex: 1 1 auto">
                        <thead>
                            <tr>
                                <th scope="col">Ime</th>
                                <th scope="col">E-mail</th>
                                <th scope="col">Akcije</th>
                            </tr>
                        </thead>                        
                        <tbody class="overflow-auto">                            
                            @foreach(App\User::all() as $user)
                            <tr>
                                <th hidden>{{ $user->id }}</th>
                                <td>{{ $user->name }}</td>
                                <td>{{ $user->email }}</td>
                                <td></td>
                            </tr>
                            @endforeach                            
                        </tbody>                        
                    </table>
                    
                </div>
            </div>
        </div>
        @endif
    </div>
</div>
@endsection
