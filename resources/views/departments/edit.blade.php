@extends('layouts.app')

@section('content')
<div class="container">
    <h1 class="md-4">Novi smjer</h1>
    <div class="row">
    <div class="col-sm-6">
    <form method="post" action="/smjerovi">
        @csrf
        @method('PATCH')
        <div class="form-group">
            <label for="name">Naziv smjera:</label>
            <input type="text" id="name" name="name" class="form-control" 
            placeholder="Unesi naziv smjera" value="{{ $department->name }}" required>
        </div>
        <div class="form-group">
            <label for="nivo_studija">Nivo studija:</label>
            <select name="nivo_studija" class="form-control">
                <option value="osnovne" selected>Osnovne</option>
                <option value="spec">Specijalističke</option>
            </select> 
        <div class="form-group">
            <button type="submit" class="btn btn-primary col-sm-2">Pošalji</button>
        </div>
    </form>
</div>
</div>
</div>
@endsection
