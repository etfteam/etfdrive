@extends('layouts.app')

@section('content')
<table class="table">
  <thead>
    <tr>
      <th scope="col">Smjer</th>
      <th scope="col">Nivo studija</th>
      <th scope="col"></th>
    </tr>
  </thead>
  <tbody>
    @foreach($departments as $department)
    <tr>
      <td>{{ $department->name }}</td>
      <td>{{ $department->nivo_studija }}</td>
      <td>
        <a class="btn btn-secondary" href="/smjerovi/{{ $department->id }}/edit"><i class="fas fa-edit"></i></a>
        <form action="/smjerovi/{{ $department->id }}"  class="d-inline" method="post">
          @csrf
          @method('DELETE')
          <button class="btn btn-danger">
          <i class="fas fa-trash"></i></button>
        </form>
      </td>
    </tr>
    @endforeach
  </tbody>
</table>
<a href="/smjerovi/create">
                <button class="btn btn-secondary my-1"><i class="fas fa-plus-circle">&nbsp;&nbsp;</i>Dodaj smjer</button>
            </a>

@endsection