@extends('layouts.app')

@section('content')
<div class="container">
    <div class="card-deck mx-2">
        <div class="row">
            <div class="d-flex flex-wrap justify-content-center justify-content-lg-start">
                <div class="col-md-6 pl-0 pr-0">
                    <div class="card m-2 px-3 bg-light" style="min-height: 23rem">
                        <div class="card-body overflow-hidden">
                            <span>
                                <small>{{ $subject->year }}. godina</small>
                                <small class="text-muted ml-2">{{ $subject->departments()->first()->name }}</small>
                            </span>
                            <h1 class="card-title my-0">{{ $subject->name }}</h1>
                            <hr class="my-0 mb-4">
                            <div class="row">
                                <div class="col-md-6">
                                    <p class="card-text" style="white-space: pre-line">{{$subject->text}}</p>    
                                </div>
                                <div class="col-md-6" style="border-left: 1px gray;">
                                    @foreach($subject->teachers()->get() as $teacher)
                                    <div class="pb-2">
                                        <span><strong>{{ $teacher->name }}</strong></span><br>
                                        <span>E-mail: {{ $teacher->email }}</span><br>
                                        <span>Kabinet: {{ $teacher->kabinet }}</span><br>
                                    </div>
                                    @endforeach
                                </div>
                            </div>                 
                            
                        </div>
                        <div class="card-footer bezok text-right bg-transparent">
                            @if(Auth::user()->admin)
                                <a href="/predmeti/{{ $subject->id }}/edit"><button class="btn btn-secondary my-1"><i class="fas fa-edit">&nbsp;&nbsp;</i>Izmijeni</button></a>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="col-md-6  pl-0 pr-0">
                    <div class="card m-2 px-3" style="flex: 1 1 25rem; height: 23rem;">
                        <div class="card-body overflow-hidden">
                            <h4 class="card-title">Obavještenja</h4>
                            <hr class="my-0 mb-3">
                            @foreach($subject->notifications()->get() as $notification)
                                <div class="m-1 mx-2 w-100 d-flex flex-row justify-content-between">
                                        <a href="/predmeti/{{ $subject->id }}/obavjestenja/{{ $notification->id }}">{{ $notification->title }}</a>
                                        <span class="text-muted">
                                            <small>
                                                {{ date('d.m.Y H:i', strtotime($notification->created_at)) }}
                                            </small>
                                        </span>
                                    </div>
                            @endforeach
                        </div>
                        @if(Auth::user()->admin)
                        <div class="card-footer bezok text-right bg-transparent">
                            <a href="/predmeti/{{$subject->id}}/obavjestenja/create"><button class="btn btn-primary">Postavi obavjestenje</button></a>
                        </div>
                        @endif
                    </div>
                </div>
                <div class="col-md-6 pl-0 pr-0">
                    <div class="card m-2 px-3" style="height: 23rem;">
                        <div class="card-body overflow-hidden">
                            <h4 class="card-title">Materijali</h4>
                            <hr class="my-0 mb-3">
                            @foreach($subject->materials()->get() as $material)
                                <div class="m-1 mx-2 w-100 d-flex flex-row justify-content-between">
                                        <a href="/predmeti/{{ $subject->id }}/materijali/{{$material->id}}">{{$material->title}}</a>
                                        <span class="text-muted">
                                            <small>
                                                {{ date('d.m.Y H:i', strtotime($material->created_at)) }}
                                            </small>
                                            
                                        </span>
                                    </div>
                            @endforeach
                        </div>
                        <div class="card-footer bezok text-right bg-transparent">
                            <a href="/predmeti/{{$subject->id}}/materijali"><button class="btn btn-primary">Prikazi vise</button></a>
                            @if(Auth::user()->admin)
                                <a href="/predmeti/{{ $subject->id }}/materijali/create"><button class="btn btn-secondary my-1"><i class="fas fa-plus-circle">&nbsp;&nbsp;</i>Dodaj materijal</button></a>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="col-md-6 pl-0 pr-0">
                    <div class="card m-2 px-3" style="flex: 1 1 25rem; height: 23rem;">
                        <div class="card-body overflow-hidden">
                            <h4 class="card-title">Pitanja</h4>
                            <hr class="my-0 mb-3">
                            @foreach($questions as $question)
                                @if($question->answer_to==null)
                                    <div class="m-1 mx-2 w-100 d-flex flex-row justify-content-between">
                                        <a href="/pitanja/{{$question->id}}">{{$question->title}}</a>
                                        <span class="text-muted">
                                            <small>
                                                {{ date('d.m.Y H:i', strtotime($question->created_at)) }}
                                            </small>
                                        </span>
                                    </div>
                                @endif
                            @endforeach
                        </div>
                        <div class="card-footer bezok text-right bg-transparent">
                            <a href="/predmeti/{{ $subject->id }}/pitanja/create"><button class="btn btn-primary">Postavi pitanje</button></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection