@extends('layouts.app')

@section('content')
<div class="col-lg-6">
    <form method="post" action="/predmeti/{{ $subject->id }}">
        @csrf
        @method('PATCH')    
        <div class="form-group">
            <label for="name">Ime predmeta:</label>
            <input value="{{ $subject->name }}" type="text" id="name" 
            name="name" class="form-control" placeholder="Unesi ime predmeta">
        </div>
        <div class="form-group">
            <label for="text">Podaci o predmetu:</label>
            <textarea rows="3" id="text" name="text" class="form-control">{{ $subject->text }}</textarea>
        </div>
        <div class="form-group">
            <label for="year">Godina studija:</label>
            <select class="form-control" id="year" name="year">
                <option @if($subject->year == 1) selected @endif>1</option>
                <option @if($subject->year == 2) selected @endif>2</option>
                <option @if($subject->year == 3) selected @endif>3</option>
                <option @if($subject->year == 4) selected @endif>4</option>
            </select>
        </div>
        <div class="form-group">
            <label for="department">Smjer:</label>
            <select class="form-control" id="department" name="department">
                @foreach($departments as $department)
                    <option 
                            value="{{ $department->id }}" 
                            @if($subject->departments()->first()->id == $department->id) selected @endif>
                        {{ $department->name }}
                    </option>
                @endforeach
            </select>
        </div>


        <button type="submit" class="btn btn-primary">Pošalji</button>
    </form>
</div>
@endsection