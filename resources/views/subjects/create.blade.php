@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
    <div class="col-sm-6">
    <form method="post" action="/predmeti">
        @csrf
        <div class="form-group">
            <label for="name">Ime predmeta:</label>
            <input type="text" id="name" name="name" class="form-control" placeholder="Unesi ime predmeta">
        </div>
        <div class="form-group">
            <label for="text">Podaci o predmetu:</label>
            <textarea rows="3" id="text" name="text" class="form-control"></textarea>
        </div>
        <div class="form-group">
            <label for="year">Godina studija:</label>
            <select class="form-control" id="year" name="year">
                <option>1</option>
                <option>2</option>
                <option>3</option>
                <option>4</option>
            </select>
        </div>
            <div class="form-group">
                <label for="department">Smjerovi:</label>
                <div id="smjerovi"></div>
                    <div class="form-inline">
                    <select class="form-control mr-1" id="department" name="department">
                        @foreach($departments as $department)
                            <option value="{{ $department->id }}">{{ $department->name }}</option>
                        @endforeach
                    </select>
                        <button class="btn btn-primary" id="dodaj_smjer">Dodaj</button>
                </div>
            </div>
        <div class="form-group">
            <button type="submit" class="btn btn-primary col-sm-2">Pošalji</button>
        </div>
    </form>
    <div class="department_template" hidden>
        <div class="department_box yeet_box">
        </div>
    </div>
</div>
</div>
</div>
@endsection

@push('head')
<script src="{{ asset('js/create_subject.js') }}"></script>
@endpush