@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
    @auth
        <div id="korisnik_ulogovan" hidden></div>
        <div class="col-lg-6">
            <h1 class="h1 mb-4">Moji predmeti</h1>
            <div class="predmeti moji_predmeti">
                <ul id="ul_predmeti"></ul>
            </div>
            <div id="predmeti-loader" class="lds-ellipsis">
                <div></div><div></div><div></div><div></div>
            </div>
                   
        </div>
    @endauth
    <div class="col-lg-6">
        <h1 class="h1 mb-4">Svi predmeti</h1>

        <button id="b1" class="btn btn-primary godina_dugme">I godina</button>
        <button id="b2" class="btn btn-primary godina_dugme">II godina</button>
        <button id="b3" class="btn btn-primary godina_dugme">III godina</button>
        <button id="b4" class="btn btn-primary godina_dugme">IV godina</button>
        @auth
        @if(Auth::user()->admin)
            <a href="/predmeti/create">
                <button class="btn btn-secondary my-1"><i class="fas fa-plus-circle">&nbsp;&nbsp;</i>Dodaj predmet</button>
            </a>
        @endif
        @endauth
        @foreach($departments as $department)
            <details class="nivo_{{ $department->nivo_studija }}" hidden>
                <summary class="ime_smjera">{{ $department->name }}</summary>
                <div class="predmeti">
                    <ul class="godina_1" hidden>
                        @foreach($department->subjects()->where('year',1)->get() as $subject)
                            <li>
                                @auth<i class="fa fa-plus">&nbsp;&nbsp;</i>@endauth
                                <span hidden>{{ $subject->id }}</span>
                                <a href="/predmeti/{{ $subject->id }}">{{ $subject->name }}</a>
                            </li>
                        @endforeach
                    </ul>
                    <ul class="godina_2" hidden>
                        @foreach($department->subjects()->where('year',2)->get() as $subject)
                            <li>
                                @auth<i class="fa fa-plus">&nbsp;&nbsp;</i>@endauth
                                <span hidden>{{ $subject->id }}</span>
                                <a href="/predmeti/{{ $subject->id }}">{{ $subject->name }}</a>
                            </li>
                        @endforeach
                    </ul>
                    <ul class="godina_3" hidden>
                        @foreach($department->subjects()->where('year',3)->get() as $subject)
                            <li>
                                @auth<i class="fa fa-plus">&nbsp;&nbsp;</i>@endauth
                                <span hidden>{{ $subject->id }}</span>
                                <a href="/predmeti/{{ $subject->id }}">{{ $subject->name }}</a>
                            </li>
                        @endforeach
                    </ul>
                    <ul class="godina_4" hidden>
                        @foreach($department->subjects()->where('year',4)->get() as $subject)
                            <li>
                                @auth<i class="fa fa-plus">&nbsp;&nbsp;</i>@endauth
                                <span hidden>{{ $subject->id }}</span>
                                <a href="/predmeti/{{ $subject->id }}">{{ $subject->name }}</a>
                            </li>
                        @endforeach
                    </ul>
                </div>
            </details>
        @endforeach
    </div>
    </div>
</div>
@endsection

@push('head')
    <script src="{{ asset('js/choose_year.js') }}"></script>
@endpush
