<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet" type="text/css">

    <!-- Styles -->
    <link href="{{ asset('css/shome.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <!--<link href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap-glyphicons.css" rel="stylesheet">-->
    <!--<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>-->
    <!--<script type="text/javascript" src="http://twitter.github.io/bootstrap/assets/js/bootstrap-transition.js"></script>
    <script type="text/javascript" src="http://twitter.github.io/bootstrap/assets/js/bootstrap-collapse.js"></script>
    <script src="//netdna.bootstrapcdn.com/twitter-bootstrap/2.3.1/js/bootstrap.min.js"></script>-->
    <!--<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css" integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">-->

    
    @stack('head')
</head>
<body>
    <div id="app">
            <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
                            <span class="navbar-toggler-icon"></span>
                    </button>
                    <div class="container">
                    <div class="collapse navbar-collapse" id="navbarNavDropdown">
                    
                      <ul class="navbar-nav">
                        <li class="nav-item active">
                          <a class="nav-link" href="/">Home</a>
                        </li>
                        <li class="nav-item active">
                          <a class="nav-link" href="/predmeti">Predmeti</a>
                        </li>
                        <li class="nav-item active">
                          <a class="nav-link" href="/pitanja">Pitanja</a>
                        </li>
                        <li class="nav-item active">
                            <a class="nav-link" href="/ankete">Ankete</a>
                        </li>
                      </ul>
                      <ul class="nav navbar-nav ml-auto">
                            <!-- Authentication Links -->
                            @guest
                                <li class="nav-item active">
                                    <a class="nav-link" href="{{ route('login') }}"> <i class="fas fa-sign-in-alt"></i> {{ __('Login') }}</a>
                                </li>
                                @if (Route::has('register'))
                                    <li class="nav-item active">
                                        <a class="nav-link" href="{{ route('register') }}"><i class="fas fa-user"></i> {{ __('Register') }}</a>
                                    </li>
                                @endif
                            @else
                                <li class="nav-item dropdown">
                                    <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                        {{ Auth::user()->name }} <span class="caret"></span>
                                    </a>
    
                                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                        <a class="dropdown-item text-dark" href="/profil">
                                           Profil
                                        </a>
                                        <a class="dropdown-item text-dark" href="{{ route('logout') }}"
                                           onclick="event.preventDefault();
                                                         document.getElementById('logout-form').submit();">
                                            Odjavi se
                                        </a>
                                        
    
                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            @csrf
                                        </form>
                                    </div>
                                </li>
                            @endguest
                        </ul>
                    </div>
                </div>
            </nav>
            {{-- <nav class="navbar navbar-fixed-top navbar-expand-lg navbar-dark bg-dark">
                    <div class="container collapse navbar-collapse">
                      <ul class="nav navbar-nav">
                        <li class="nav-item active"><a href="#">Home</a></li>
                        <li class="nav-item active"><a href="#">Predmeti</a></li>
                        <li class="nav-item active"><a href="#">Pitanja</a></li>
                        <li class="nav-item active"><a href="#">Materijali</a></li>
                        <li class="nav-item active"><a href="#">Ankete</a></li>
                      </ul> --}}
                      {{-- <ul class="nav navbar-nav navbar-right">
                        <li class="nav-item active"><a href="#"><span class="glyphicon glyphicon-user"></span> Sign Up</a></li>
                        <li class="nav-item active"><a href="#"><span class="glyphicon glyphicon-log-in"></span> Login</a></li>
                      </ul>
                    </div> --}}
                    <!-- Right Side Of Navbar -->
        <main class="py-4 container">
            @yield('content')
        </main>
    </div>
</body>
</html>
