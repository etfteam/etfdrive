@extends('layouts.app')
@section('content')
{{-- <div class="container">
<div class="row">
    <div class="col-sm-6">
    @foreach($surveys as $survey)
        <h4 class="mt-3">{{ $survey->title }}</h3>
        <p>{{ $survey->description }}</p>
        <a href="ankete/{{ $survey->id }}" class="btn btn-primary">Ispuni</a>
    @endforeach
    </div>
</div>
</div> --}}
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
        <h1 class="mb-4">Ankete</h1>
        @foreach($surveys as $survey)
        <!--<div class="col-sm-4">-->    
            <div class="card my-2 px-3" style="height: 12rem;">
                <div class="card-body overflow-hidden">
                    <span>                      
                        <small class="">
                            {{ date('d.m.Y H:i', strtotime($survey->created_at)) }}</small>
                    </span>
                    <hr class="my-0 mb-2">
                    <h3 class="card-title" style="color: #d66e0a">{{ $survey->title }}</h3>
                    <span style="white-space: pre-line">{{ $survey->description }}</span>                   
                </div>
                <div class="card-footer bezok text-right bg-transparent">
                    <a href="/ankete/{{ $survey->id }}" class="btn btn-primary">
                        @if($survey->survey_answers()->where('user_id', Auth::id())->exists())
                        Prikaži rezultate
                        @else
                        Ispuni
                        @endif
                    </a>
                </div>
            </div>
        @endforeach
        @if(Auth::user()->admin)
            <div class="card my-2" style="flex: 1 1 25rem; height: 12rem">
                <div class="card-body col d-flex flex-column justify-content-center 
                align-items-center bg-lightgray">
                    <a href="/ankete/create" class="display-3 text-secondary"><i class="fas fa-plus"></i></a>
                </div>
            </div>
        @endif
    </div>
</div>
</div>
@endsection
<!--<a href="ankete/" class="btn btn-primary m-2 mt-auto w-50 align-self-end">
                    Ispuni</a>-->