@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row">
        <div class="col-sm-6 w-100">
            <div class="card bg-lightgray">
                <div class="card-body">
                    <h1 class="md-5">{{ $survey->title }}</h1>
                    <span style="white-space: pre-line">{{ $survey->description }}</span>                   
                </div>
            </div>
        </div>
        <div class="col-sm-6 h-100-vh overflow-auto">
            @foreach($questions as $question)
                <form class="answer_form">
                    @csrf
                    <div class="tip_odgovora" hidden>{{ $question->type }}</div>
                    <input type="hidden" name="survey_question_id" value="{{ $question->id }}" class="form-control">
                    <div class="form-group form-group-lg">
                        <label class="form-control shadow-none bezok mr-2" for="question[{{ $question->id }}]">{{ $question->text }}</label>
                    </div>
                    <div class="form-group form-group-lg">
                        @if($question->type=='text')
                            <input
                                name="answer"
                                type="text"
                                id="question[{{ $question->id }}]"
                                placeholder="Odgovor" class="form-control">
                            
                        @else
                            @foreach($question->choices()->get() as $choice)
                                <div class="radio">
                                    <label><input type="radio" value="{{ $choice->choice }}" class="ml-5" name="answer">&nbsp;&nbsp;{{ $choice->choice }}</label>
                                </div>
                            @endforeach
                        @endif
                    </div>
                    <hr>
                </form>
            @endforeach
            <div class="form-group text-right">
                <button id="btn_submit_answers" class="btn btn-primary col-sm-3  mt-2">Posalji</button>
                <div id="loader" class="lds-ellipsis" hidden>
                <div></div><div></div><div></div><div></div>
            </div>
            </div>
        </div>
        
    </div>
</div>
@endsection

@push('head')
    <script src="{{ asset('js/answer_survey.js') }}"></script>
@endpush