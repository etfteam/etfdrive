@extends('layouts.app')
@section('content')
    <div class="container">
            <div class="row">
                <div class="col-sm-6">
                    <h1 class="h1 mb-4">Kreiraj anketu</h1>
                    <form id="survey" method="post" action="/ankete">
                        @csrf
                        <div class="form-group form-group-lg">
                            <input type="text" name="title" placeholder="Naslov ankete" class="form-control">
                        </div>
                        <div class="form-group form-group-lg">
                            <textarea name="description" placeholder="Opis ankete" class="form-control"></textarea>
                        </div>
                    </form>
                    <div class="form-group form-row">
                        <label for="tip_pitanja" class="form-text col-sm-12">Odaberi tip pitanja:</label>
                        <div class="col-sm-9 pr-2">
                            <select class="form-control" id="tip_pitanja">
                            <option value="text" selected>Tekstualni odgovor</option>
                            <option value="multi">Ponuđeni odgovori</option>
                            </select>    
                        </div>
                        
                        <button id="btn_add_question" class="btn btn-secondary col-sm-3">
                        Dodaj pitanje</button>
                    </div>
                    <div class="form-group">
                        <button id="btn_submit" class="btn btn-primary col-sm-3">Pošalji</button>
                        <div id="sending_loader" class="lds-ellipsis" hidden>
                            <div></div><div></div><div></div><div></div>
                        </div>
                    </div>
                    <div id="question_form_template" hidden>
                        <form class="question_form">
                            @csrf
                            <div class="form-row">
                                <div class="form-group col-sm-10">
                                    <input id="question_field" type="text" name="text" class="bg-lightgray form-control">
                                    <input type="hidden" name="type" value="text" class="form-control">
                                </div>
                                <div class="col-sm-2">
                                    <button class="btn btn-danger" id="btn_remove_question"><i class="fas fa-trash"></i></button>
                                </div>
                            </div>
                        <hr>
                        </form>
                        
                    </div>
                    <div id="question_form_template_multi" hidden>
                        <form class="question_form">
                            @csrf
                            <div class="form-row">
                                <div class="form-group col-sm-10">
                                    <input id="question_field" type="text" name="text" class="bg-lightgray form-control">
                                    <input type="hidden" name="type" value="multi" class="form-control">
                                    <label for="choices" class="form-text">Unesi ponuđene odgovore:</label>
                                    <div class="ponudjeni_odgovori">
                                        <button class="btn btn-secondary">Dodaj odgovor</button>
                                    </div>
                                </div>
                                <div class="col-sm-2">
                                    <button class="btn btn-danger" id="btn_remove_question"><i class="fas fa-trash"></i></button>
                                </div>
                            </div>
                        <hr>
                        </form>
                        
                    </div>
                </div>
                <div id="questions" class="col-sm-6 h-100-vh overflow-auto">
                    <h1 class="h1 mb-4">Pitanja</h1>
                </div>
            </div>
        </div>
@endsection

@push('head')
<script src="{{ asset('js/create_survey.js') }}"></script>
@endpush