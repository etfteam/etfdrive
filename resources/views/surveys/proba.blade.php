@extends(layouts.app)
@section('content')

    <div class="container">
        <div class="row">
            <div class="col-sm-6">
                <form id="survey" method="post" action="/ankete">
                    <div class="form-group">
                        @csrf
                        <input type="text" name="title" placeholder="Naslov ankete" class="form-control">
                        <textarea name="description" placeholder="Opis ankete" class="form-control"></textarea>
                    </div>
                    <div>
                        <button id="btn_add_question" class="btn btn-primary">Dodaj pitanje</button>
                        <button id="btn_submit" class="btn btn-primary">Posalji</button>
                    </div>
                    <div class="form-group">
                        @csrf
                        <input id="question_field" type="text" name="text" class="form-control">
                        <input type="hidden" name="type" value="text" class="form-control">
                        <button class="btn btn-danger" id="btn_remove_question">Remove</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

@endsection

@push('head')
<script src="{{ asset('js/create_survey.js') }}"></script>
@endpush