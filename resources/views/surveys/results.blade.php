@extends('layouts.app')

@section('content')
<div class="container">
  <!--<span class="text-muted small">Rezultati ankete</span>-->
  <h1>{{ $survey->title }}</h1>
  <span style="white-space: pre-line;">{{ $survey->description }}</span>
    <div class="row mt-4">
      @foreach($questions as $question)
      <div class="col-md-6">
        @if($question->type=="multi")
        <div class="card px-3" style="height: 27rem">
          <div class="card-body">
            <h4>{{ $question->text }}</h4>
            <hr class="mt-0 mb-3">
          <canvas id="myChart{{ $question->id }}"></canvas>
          <script>
            var ctx = document.getElementById('myChart{{ $question->id }}').getContext('2d');
            var myChart = new Chart(ctx, {
                type: 'bar',
                data: {
                    labels: [ 
                      @foreach($question->choices()->pluck('choice')->toArray() as $choice_label)
                        '{{ $choice_label }}',
                      @endforeach
                    ],
                    datasets: [{
                        label: '',
                        data: [
                            @foreach($question->choices()->get() as $choice)
                                {{ count($question->survey_answers()->where('answer',$choice->choice)->get()) }},
                            @endforeach
                        ],
                        /*backgroundColor: [
                            'rgba(255, 99, 132, 0.2)',
                            'rgba(54, 162, 235, 0.2)',
                            'rgba(255, 206, 86, 0.2)',
                            'rgba(75, 192, 192, 0.2)',
                            'rgba(153, 102, 255, 0.2)',
                            'rgba(255, 159, 64, 0.2)'
                        ],
                        borderColor: [
                            'rgba(255, 99, 132, 1)',
                            'rgba(54, 162, 235, 1)',
                            'rgba(255, 206, 86, 1)',
                            'rgba(75, 192, 192, 1)',
                            'rgba(153, 102, 255, 1)',
                            'rgba(255, 159, 64, 1)'
                        ],*/
                        borderWidth: 1
                    }]
                },
                options: {
                  aspectRatio: 1.33,
                  responsive: true,
                  legend: {
                      display: false
                  },
                    scales: {
                        yAxes: [{
                            ticks: {
                                beginAtZero: true,
                            }
                        }]
                    }
                }
            });
          </script>
          </div>
        </div>
        @else
          <div class="card px-3">
            <div class="card-body" style="height: 27rem">
              <h4>{{ $question->text }}</h4>
              <hr class="mt-0 mb-3">
              <ul>
                @foreach($question->survey_answers()->get() as $answer)
                <li>{{ $answer->answer }}</li>
                @endforeach
              </ul>
            </div>
          </div>
        @endif
      </div>
      @endforeach
  </div>
</div>
@endsection

@push('head')
    <script src="https://cdn.jsdelivr.net/npm/chart.js@2.8.0"></script>
@endpush