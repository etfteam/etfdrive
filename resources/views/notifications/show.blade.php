@extends('layouts.app')

@section('content')
<div class="container">
  <div class="justify-content-center row">
    <div class="col-sm-8">
      <div class="card col-sm-12 mt-2">
        <div class="card-body">
          <h4 class="card-title">{{$notification->title}}</h4>
          <p class="card-text">{{$notification->text}}</p>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection