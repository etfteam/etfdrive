@extends('layouts.app')

@section('content')
<div class="container">
    <h1 class="mb-4">Novo obavještenje<span class="text-muted"> - {{ $subject->name }}</span></h1>
    <div class="row">    
    <div class="col-sm-6">    
    <form method="post" action="/predmeti/{{ $subject->id }}/obavjestenja">
        @csrf
        <div class="form-group">
            <label for="title">Naslov:</label>
            <input type="text" id="title" name="title" class="form-control" 
            placeholder="Unesi naslov obavještenja" required>
        </div>
        <div class="form-group">
            <label for="text">Tekst:</label>
            <textarea class="form-control" name="text"></textarea>
        </div>
        <div class="form-group">
            <button type="submit" class="btn btn-primary col-sm-2">Pošalji</button>
        </div>
    </form>
</div>
</div>
</div>
@endsection