@extends('layouts.app')

@section('content')
<h1 class="h1 mb-4 color_accent">{{ $subject->name }} - Obavještenja</h1>
<table class="table">
  <thead>
    <tr>
      <th scope="col">Datum</th>
      <th scope="col">Naslov</th>
      <th scope="col">Opis</th>      
    </tr>
  </thead>
  <tbody>
    @foreach($notifications as $notification)
    <tr><a href="/predmeti/{{ $subject->id }}/obavjestenja/{{ $notification->id }}">
      <td>{{ \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', 
      $notification->created_at)->format('d.m.Y') }}</td>
      <td>{{ $notification->title }}</td>
      <td>{{ $material->text }}</td>
    </tr></a>
    @endforeach
  </tbody>
</table>
@endsection