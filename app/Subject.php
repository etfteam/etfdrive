<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Subject extends Model
{
    protected $guarded=[];

    public function departments() {
        return $this->belongsToMany('App\Department');
    }

    public function teachers() {
        return $this->belongsToMany('App\Teacher');
    }

    public function notifications() {
        return $this->hasMany('App\Notification');
    }

    public function materials() {
        return $this->hasMany('App\Material');
    }

    public function questions() {
        return $this->hasMany('App\Question');
    }
}
