<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Attachment extends Model
{
    protected $guarded=[];

    public function question() {
        return $this->belongsTo('App\Question');
    }
}
