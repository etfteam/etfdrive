<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SurveyAnswer extends Model
{
    protected $guarded=[];

    public function survey_question() {
        return $this->belongsTo('App\SurveyQuestion');
    }

    public function user() {
        return $this->belongsTo('App\User');
    }
}
