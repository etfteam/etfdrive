<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SurveyQuestion extends Model
{
    protected $guarded=[];

    public function survey () {
        return $this->belongsTo('App\Post');
    }

    public function survey_answers () {
        return $this->hasMany('App\SurveyAnswer');
    }

    public function choices () {
        return $this->hasMany('App\Choice');
    }
}
