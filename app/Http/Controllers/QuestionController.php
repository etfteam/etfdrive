<?php

namespace App\Http\Controllers;

use App\Question;
use App\Subject;
use App\Attachment;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use DB;

class QuestionController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $questions=Question::all();
        return view('questions.index', compact('questions'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Subject $subject)
    {
        return view('questions.create', compact('subject'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'title'=>'required',
            'text'=>'required',
            'fajl'=>'nullable'
        ]);
        if($request->hasFile('fajl')){
            $filenameWithExt=$request->file('fajl')->getClientOriginalName();
            $filename=pathinfo($filenameWithExt,PATHINFO_FILENAME);
            $extension=$request->file('fajl')->getClientOriginalExtension();
            $fileNameToStore=$filename.'_'.time().'.'.$extension;
            $path=$request->file('fajl')->storeAs('pitanja/',$fileNameToStore);
        }
        else{
            $fileNameToStore='no-image.jpg';
        }
        $post=new Question;
        $post->title=$request->input('title');
        $post->text=$request->input('text');
        $post->user_id=auth()->user()->id;
        $post->subject_id=$request->subject_id;
        $post->path=$fileNameToStore;
        $post->save();

        if($request->hasFile('fajl')){
            $attachment=new Attachment;
            $attachment->path = $path;
            $attachment->question_id = $post->id;
            $attachment->save();
            
        }
        return redirect('pitanja')->with('success','Pitanje kreirano');
    }
    /**
     * Display the specified resource.
     *
     * @param  \App\Question  $question
     * @return \Illuminate\Http\Response
     */
    function show($question_id)
    {
        $question = Question::find($question_id);
        $answers = $question->answers()->get();
        $attachments = $question->attachments()->get();
        return view ('questions.show', compact('answers','question','attachments'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Question  $question
     * @return \Illuminate\Http\Response
     */
    public function edit(Question $question)
    {
        return view('questions.edit', compact('question'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Question  $question
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Question $question)
    {
        $this->validate($request,[
            'title'=>'required',
            'body'=>'required',
            'fajl'=>'nullable'
        ]);

        if($request->hasFile('fajl')){
            $filenameWithExt=$request->file('fajl')->getClientOriginalName();
            $filename=pathinfo($filenameWithExt, PATHINFO_FILENAME);
            $extension=$request->file('fajl')->getClientOriginalExtension();
            $fileNameToStore=$filename.'_'.time().'.'.$extension;
            $path=$request->file('fajl')->storeAs('public/fajlovi',$fileNameToStore);
        }

        $post=Question::find($id);
        $post->title=$request->input('title');
        $post->text=$request->input('body');
        if($request->hasFile('fajl')){
            $post->path=$fileNameToStore;
        }
        $post->save();
        return redirect('/pitanja')->with('success','Pitanje azurirano');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Question  $question
     * @return \Illuminate\Http\Response
     */
    public function destroy($question_id)
    {
        $question=Question::find($question_id);
        $question->delete();
        return redirect('/pitanja');
    }

    public function storeReply(Request $request){
        $answer = new Question;
        $answer->title = $request->title;
        $answer->text = $request->text;
        $answer->user_id = auth()->id();
        $answer->subject_id = $request->subject_id;
        $answer->answer_to = $request->answer_to;
        $answer->save();
        return $answer; 
    }
}
