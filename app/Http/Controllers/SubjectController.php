<?php

namespace App\Http\Controllers;

use App\Department;
use App\Subject;
use App\Survey;
use Illuminate\Http\Request;

class SubjectController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth')->except('index');
        $this->middleware('admin')
            ->except(['index', 'show','mojiPredmeti',
                'dodajMojPredmet','ukloniMojPredmet']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(){
        $departments = Department::all();
        return view('subjects.index',
            compact('departments'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $departments = Department::all();
        return view('subjects.create', compact('departments'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $subject = new Subject();
        $subject->name = $request->name;
        $subject->text = $request->text;
        $subject->year = (int)$request->year;
        $subject->save();
        
        $subject->departments()->attach($request->departments);
        return redirect('/predmeti');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Subject  $subject
     * @return \Illuminate\Http\Response
     */
    public function show($subject_id)
    {
        $subject=Subject::find($subject_id);
        $questions=$subject->questions()->get();
        $notifications=$subject->notifications()->get();
        return view('subjects.show', compact('subject'), compact('questions'), compact('notifications'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Subject  $subject
     * @return \Illuminate\Http\Response
     */
    public function edit($subject_id)
    {
        $subject= Subject::find($subject_id);
        $departments = Department::all();
        return view('subjects.edit', compact('departments','subject'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Subject  $subject
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $subject_id)
    {
        $subject = Subject::find($subject_id);
        $subject->name = $request->name;
        $subject->text = $request->text;
        $subject->year = (int)$request->year;
        $subject->update();
        return redirect('/predmeti/'.$subject_id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Subject  $subject
     * @return \Illuminate\Http\Response
     */
    public function destroy(Subject $subject)
    {
        //
    }
    public function mojiPredmeti() {
        return auth()->user()->subjects()->get()->toArray();        
    }
    public function dodajMojPredmet(Request $request) {
        $subjectId = intval($request->id);
        return auth()->user()->subjects()->syncWithoutDetaching($subjectId);
    }
    public function ukloniMojPredmet(Request $request) {
        $subjectId = intval($request->id);
        return auth()->user()->subjects()->detach($subjectId);
    }
}
