<?php

namespace App\Http\Controllers;

use App\Material;
use App\Subject;
use App\Department;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Auth;

class MaterialController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('admin')->except(['index', 'show']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Subject $subject){
        $materials = $subject->materials()->get();
        return view('materials.index', compact('materials'), compact('subject'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Subject $subject){
        return view('materials.create', compact('subject'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $material = new Material();
        $material->title = $request->title;
        $material->description = $request->description;
        $subject = Subject::find($request->subject_id);
        
        $file=$request->file('attachment');
        $filenameWithExt=$file->getClientOriginalName();
        $filename=pathinfo($filenameWithExt,PATHINFO_FILENAME);
        $extension=$file->getClientOriginalExtension();
        $fileNameToStore=$filename.'_'.time().'.'.$extension;
        $path=$file->storeAs('materijali/'.$subject->year.'/'.$subject->name,$fileNameToStore);
        $material->type = $file->getMimeType();
        $material->path = $path;
        
        $material->user_id = Auth::id();
        $material->subject_id = $request->subject_id;
        $material->save();
        return redirect('/predmeti');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Material  $material
     * @return \Illuminate\Http\Response
     */
    public function show($subject_id, $material_id) {
        $material=Material::find($material_id);
        return Storage::download($material->path);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Material  $material
     * @return \Illuminate\Http\Response
     */
    public function edit(Subject $subject, Material $material)
    {
        return view('materials.edit', compact('material'), compact('subject'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Material  $material
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Material $material)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Material  $material
     * @return \Illuminate\Http\Response
     */
    public function destroy(Material $material)
    {
        //
    }
}
