<?php

namespace App\Http\Controllers;

use App\Notification;
use App\Subject;
use Illuminate\Http\Request;

class NotificationController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('admin')->except(['index', 'show']);
    }    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($subject_id) {
        $subject = Subject::find($subject_id);
        $notifications = $subject->notifications();
        return view('notifications.index', compact('notifications'),
            compact('subject'));        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($subject_id)
    {
        $subject=Subject::find($subject_id);
        return view('notifications.create', compact('subject'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store($subject_id, Request $request)
    {
        $validated=$request->validate([
            'title'=>'required',
            'text'=>'required'
        ]);
        $validated['user_id']=auth()->id();
        $validated['subject_id']=$subject_id;
        Notification::create($validated);
        return redirect('/predmeti/'.$subject_id);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Notification  $notification
     * @return \Illuminate\Http\Response
     */
    public function show($subject_id, $notification_id)
    {
        $notification=Notification::find($notification_id);
        return view('notifications.show', compact('notification'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Notification  $notification
     * @return \Illuminate\Http\Response
     */
    public function edit($subject_id, $notification_id)
    {
        $subject=Subject::find($subject_id);
        $notification=Notification::find($notification_id);
        return view('notifications.edit', compact('notification'), compact('subject'));       
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Notification  $notification
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $subject_id, $notification_id)
    {
        $notification=Notification::find($notification_id);
        $validated=$request->validate([
            'title'=>'required',
            'text'=>'required'
        ]);
        $validated['user_id']=Auth::id();
        $validated['subject_id']=$subject_id;
        $notification->update($validated);
        return redirect('/predmeti/'.$subject_id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Notification  $notification
     * @return \Illuminate\Http\Response
     */
    public function destroy($subject_id, $notification_id)
    {
        $notification=Notification::find($notification_id);
        $notification->delete();
        return redirect('/predmeti/'.$subject_id);
    }
}
