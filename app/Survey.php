<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Survey extends Model
{
    protected $guarded=[];

    public function survey_questions() {
        return $this->hasMany('App\SurveyQuestion');
    }

    public function survey_answers() {
        return $this->hasManyThrough('App\SurveyAnswer', 'App\SurveyQuestion');
    }
}
