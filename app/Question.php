<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
    protected $guarded=[];

    public function user() {
        return $this->belongsTo('App\User');
    }
    public  function subject() {
        return $this->belongsTo('App\Subject');
    }
    public function question() {
        return $this->belongsTo('App\Question', 'answer_to');
    }
    public function answers() {
        return $this->hasMany('App\Question', 'answer_to');
    }
    public function attachments() {
        return $this->hasMany('App\Attachment');
    }
    public function upvotes() {
        return $this->hasMany('App\Upvote');
    }
}
