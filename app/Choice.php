<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Choice extends Model
{
    protected $guarded =[];
    public function survey_question(){
        return $this->belongsTo('App\SurveyQuestion');
    } 
}
