const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.react('resources/js/app.js', 'public/js')
    .js('resources/js/create_survey.js','public/js')
    .js('resources/js/choose_year.js','public/js')
    .js('resources/js/answer_survey.js','public/js')
    .js('resources/js/create_subject.js','public/js')
    .js('resources/js/update_icons.js','public/js')
    .js('resources/js/answer_question.js', 'public/js')
    .sass('resources/sass/app.scss', 'public/css');

